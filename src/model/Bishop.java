package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bishop extends Piece {
	public Bishop(String imagem, String tipo) {
		super(imagem, tipo);
	}
	public Bishop(String imagem) {
		super(imagem);
	}
	
	public ArrayList<Point> getMover(int x, int y) {
		ArrayList<Point> moverBishop = new ArrayList<>();
		for(int i = x, j = y;( i < 7 || j < 7) ; i++, j++) {
			if(i == 7 || j == 7) {
				break;
			}
			Point pointBDI = new Point(i + 1, j + 1);
			moverBishop.add(pointBDI);
		}
		for(int i = x, j = y;( i < 7 || j > 0) ; i++, j--) {
			if(i == 7 || j == 0) {
				break;
			}
			Point pointBEI = new Point(i + 1, j - 1);
			moverBishop.add(pointBEI);
		}
		for(int i = x, j = y;( i > 0 || j < 7) ; i--, j++) {
			if(i == 0 || j == 7) {
				break;
			}
			Point pointBES = new Point(i - 1, j + 1);
			moverBishop.add(pointBES);
		}
		for(int i = x, j = y;( i > 0 || j > 0) ; i--, j--) {
			if(i == 0 || j == 0) {
				break;
			}
			Point pointBDS = new Point(i - 1, j - 1);
			moverBishop.add(pointBDS);
		}
		return moverBishop;
	}
}
