package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {
	public Rook(String imagem, String tipo) {
		super(imagem, tipo);
	}
	public Rook(String imagem) {
		super(imagem);
	}

	public ArrayList<Point> getMover(int x, int y) {
		ArrayList<Point> moverRook = new ArrayList<>();
		for(int i = y + 1; i < 8; i++) {
			Point pointBDS = new Point(x, i);
			moverRook.add(pointBDS);
		}
		for(int i = y - 1; i >= 0; i--) {
			Point pointBES = new Point(x, i);
			moverRook.add(pointBES);
		}
		for(int i = x + 1; i < 8; i++) {
			Point pointBDI = new Point(i, y);
			moverRook.add(pointBDI);
		}
		for(int i = x - 1; i >= 0; i--) {
			Point pointBEI = new Point(i, y);
			moverRook.add(pointBEI);
		}
		return moverRook;
	}
}
