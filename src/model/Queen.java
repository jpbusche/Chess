package model;

import java.awt.Point;
import java.util.ArrayList;

public class Queen extends Piece {

	public Queen(String imagem, String tipo) {
		super(imagem, tipo);
	}

	public Queen(String imagem) {
		super(imagem);
	}
	
	public ArrayList<Point> getMover(int x, int y) {
		ArrayList<Point> moverQueen = new ArrayList<>();
		for(int i = y+1; i < 8; i++) {
			Point pointT1 = new Point(x, i);
			moverQueen.add(pointT1);
		}
		for(int i = y-1; i >= 0; i--) {
			Point pointT2 = new Point(x, i);
			moverQueen.add(pointT2);
		}
		for(int i = x+1; i < 8; i++) {
			Point pointT3 = new Point(i, y);
			moverQueen.add(pointT3);
		}
		for(int i = x-1; i >= 0; i--) {
			Point pointT4 = new Point(i, y);
			moverQueen.add(pointT4);
		}
		for(int i = x, j = y; (i < 7 || j < 7); i++, j++) {
			if(i == 7 || j == 7) {
				break ;
			}
			Point pointB1 = new Point(i+1, j+1);
			moverQueen.add(pointB1);
		}
		for(int i = x, j = y; (i < 7 || j > 0); i++, j--) {
			if(i == 7 || j == 0) {
				break ;
			}
			Point pointB2 = new Point(i+1, j-1);
			moverQueen.add(pointB2);
		}
		for(int i = x, j = y; (i > 0 || j < 7); i--, j++) {
			if(i == 0 || j == 7) {
				break;
			}
			Point pointB3 = new Point(i-1, j+1);
			moverQueen.add(pointB3);
		
		}
		for(int i = x, j = y; (i > 0 || j > 0); i--, j--) {
			if(i == 0 || j == 0) {
				break;
			}
			Point pointB4 = new Point(i-1, j-1);
			moverQueen.add(pointB4);
		}
		return moverQueen;
	}	
}
