package model;

import java.awt.Point;
import java.util.ArrayList;
import javax.swing.text.Position;

public abstract class Piece {
	private String tipo;
	private String imagem;
	private ArrayList<Position> mover;
	
	public Piece(String imagem) {
		super();
		this.imagem = imagem;
	}
	public Piece(String imagem, String tipo) {
		super();
		this.imagem = imagem;
		this.tipo = tipo;
	}
	public String getTipo() {
		return tipo;
	}
	public String getImagem() {
		return imagem;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;	
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public ArrayList<Position> getMover() {
		return mover;
	}
	public void setMover(ArrayList<Position> mover) {
		this.mover = mover;
	}
	public ArrayList<Point> getMoves(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMovesY(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMovesB(int x, int y) {
		return null;
	}
}