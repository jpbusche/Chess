package model;

import java.awt.Point;
import java.util.ArrayList;

public class Pawn extends Piece {
	public Pawn(String imagem, String tipo) {
		super(imagem, tipo);
	}
	public Pawn(String imagem) {
		super(imagem);
	}
	
	public ArrayList<Point> getMoverB(int x, int y) {
		ArrayList<Point> moverPawn = new ArrayList<>();
		if(x == 1) {
			Point pointF = new Point(x + 1, y);
			moverPawn.add(pointF);
			Point pointFF = new Point(x + 2, y);
			moverPawn.add(pointFF);
			Point pointE = new Point(x + 1, y + 1);
			moverPawn.add(pointE);
			Point pointD = new Point(x + 1, y - 1);
			moverPawn.add(pointD);
		}
		else {
			Point pointE = new Point(x + 1, y + 1);
			moverPawn.add(pointE);
			Point pointD = new Point(x + 1, y - 1);
			moverPawn.add(pointD);
			Point pointF = new Point(x + 1, y);
			moverPawn.add(pointF);
		}
		return moverPawn;
	}
	public ArrayList<Point> getMoverY(int x, int y) {
		ArrayList<Point> moverPawn = new ArrayList<>();
		if(x == 6) {
			Point pointF = new Point(x - 1, y);
			moverPawn.add(pointF);
			Point pointFF = new Point(x - 2, y);
			moverPawn.add(pointFF);
			Point pointE = new Point(x - 1, y + 1);
			moverPawn.add(pointE);
			Point pointD = new Point(x - 1, y - 1);
			moverPawn.add(pointD);
		}
		else {
			Point pointE = new Point(x - 1, y + 1);
			moverPawn.add(pointE);
			Point pointD = new Point(x - 1, y - 1);
			moverPawn.add(pointD);
			Point pointF = new Point(x - 1, y);
			moverPawn.add(pointF);
		}
		return moverPawn;
	}
}
