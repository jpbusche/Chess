package model;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece {
	public Knight(String imagem) {
		super(imagem);
	}
	public Knight(String imagem, String tipo) {
		super(imagem, tipo);
	}
	
	public ArrayList<Point> getMover(int x, int y) {
		ArrayList<Point> moverKnight = new ArrayList<>();
		if(!(y == 6 || y == 7)) {
			if(!(x == 0)) {
				Point pointDC = new Point(x-1, y+2);
				moverKnight.add(pointDC);
			}
			if(!(x == 7)) {
				Point pointDB = new Point(x+1, y+2);
				moverKnight.add(pointDB);
			}
		}
		if(!(y == 0 || y == 1)) {
			if(!(x == 0)) {
				Point pointEC = new Point(x-1, y-2);
				moverKnight.add(pointEC);
			}
			if(!(x == 7)) {
				Point pointEB = new Point(x+1, y-2);
				moverKnight.add(pointEB);
			}
		}
		if(!(x == 0 || x == 1)) {
			if(!(y == 7)) {
				Point pointCD = new Point(x-2, y+1);
				moverKnight.add(pointCD);
			}
			if(!(y == 0)) {
				Point pointCE = new Point(x-2, y-1);
				moverKnight.add(pointCE);
			}
		}
		if(!(x == 7 || x == 6)) {
			if(!(y == 7)) {
				Point pointBD = new Point(x+2, y+1);
				moverKnight.add(pointBD);
			}
			if(!(y == 0)) {
				Point pointBE = new Point(x+2, y-1);
				moverKnight.add(pointBE);
			}
		}
		return moverKnight;
	}
}