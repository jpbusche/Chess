package model;

public class outOfPossibleSquaresException extends Exception {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	
	public outOfPossibleSquaresException() { 
		super(); 
	}
	public outOfPossibleSquaresException(String message) { 
		super(message); 
	}
	public outOfPossibleSquaresException(String message, Throwable cause) {
		super(message, cause); 
	}
	public outOfPossibleSquaresException(Throwable cause) { 
		super(cause); 
	}	
	
}
