package model;

import java.awt.Point;
import java.util.ArrayList;

public class King extends Piece{
	public King(String imagem, String tipo) {
		super(imagem, tipo);
	}
	public King(String imagem) {
		super(imagem);
	}

	public ArrayList<Point> getMover(int x, int y) {
		ArrayList<Point> moverKing = new ArrayList<>();
		if(!(x == 0)) {
			Point pointC = new Point(x-1, y);
			moverKing.add(pointC);
			if(!(y == 7)) {
				Point pointCD = new Point(x-1, y+1);
				moverKing.add(pointCD);
			}
			if(!(y == 0)) {
				Point pointCE = new Point(x-1, y-1);
				moverKing.add(pointCE);
			}
		}
		if(!(x == 7)) {
			Point pointB = new Point(x+1, y);
			moverKing.add(pointB);
			if(!(y == 7)) {
				Point pointBD = new Point(x+1, y+1);
				moverKing.add(pointBD);
			}
			if(!(y == 0)) {
				Point pointBE = new Point(x+1, y-1);
				moverKing.add(pointBE);
			}
		}
		if(!(y == 7)) {
			Point pointD = new Point(x, y+1);
			moverKing.add(pointD);
		}
		if(!(y == 0)) {
			Point pointE = new Point(x, y-1);
			moverKing.add(pointE);
		}
		return moverKing;
	}
}
