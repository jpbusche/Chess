package util;

import java.util.Map;
import java.util.TreeMap;

import javax.swing.ImageIcon;

public class ImageHandler {

	private static ImageHandler instance;

	private Map<String, ImageIcon> iconMap;

	private ImageHandler() {
		this.iconMap = new TreeMap<String, ImageIcon>();
	}

	public static ImageHandler getInstance() {
		if (instance == null) {
			instance = new ImageHandler();
		}
		return instance;
	}

	public static ImageIcon load(String imagem) {
		return ImageHandler.getInstance().loadImage(imagem);
	}

	public ImageIcon loadImage(String imagem) {
		if (!haveImage(imagem)) {
			this.iconMap.put(imagem, new ImageIcon(imagem));
			
		}	
		
		return this.iconMap.get(imagem);
	}

	public boolean haveImage(String imagem) {
		return this.iconMap.containsKey(imagem);
	}

}
