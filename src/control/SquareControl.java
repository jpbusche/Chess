package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Bishop;
import model.King;
import model.Queen;
import model.Piece;
import model.Pawn;
import model.Rook;
import model.Knight;
import model.Square;
import model.outOfPossibleSquaresException;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVER = new Color(0x66B0FF);

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMover;

	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_MOVER);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMover) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMover = colorMover;

		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) throws outOfPossibleSquaresException {
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square)) {
				//throw new outOfPossibleSquaresException("QUADRADO INDISPONIVEL");
				JOptionPane.showMessageDialog(null, "Movimentação Errada!Tente Novamente", "Movimentação Invalida", JOptionPane.INFORMATION_MESSAGE);
			}
			if(!this.selectedSquare.equals(square) && possibleSquares.contains(square)) {
				moveContentOfSelectedSquare(square);
				possibleSquares.clear();
			}
			else {
				unselectSquare(square);
			}
		} else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && !possibleSquares.contains(square)) {
			resetColor(square);
		} 
		else {
			if(this.selectedSquare == square) 
				square.setColor(this.colorSelected);
			else
				square.setColor(new Color(0x66B0FF));	
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showPossibleMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		for(int i = 0; i<possibleSquares.size(); i++)
			resetColor(possibleSquares.get(i));
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	private void showPossibleMovesPawnB(Square square) {
		Pawn pawn = (Pawn) square.getPiece();
		ArrayList<Point> moverPawn = pawn.getMoverB(square.getPosition().x, square.getPosition().y);
		for(int i = 0; i < moverPawn.size(); i++) {
			Square squares = getSquare(moverPawn.get(i).x, moverPawn.get(i).y);
			Square squareE = getSquare(square.getPosition().x+1, square.getPosition().y+1);
			Square squareD = getSquare(square.getPosition().x+1, square.getPosition().y-1);
			Square squareF = getSquare(square.getPosition().x+1, square.getPosition().y);
			if((square.getPosition().y != 0 && squares == squareD && squareD.havePiece()) || (square.getPosition().y != 7 && squares == squareE && squareE.havePiece())) {
				if(squares.getPiece().getImagem().contains("Yellow")) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareD && squares != squareE) {
				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
			}
		}
	}
	private void showPossibleMovesPawnY(Square square) {
		Pawn pawnY = (Pawn) square.getPiece();
		ArrayList<Point> moverPawn = pawnY.getMoverY(square.getPosition().x, square.getPosition().y);
		for(int i = 0; i < moverPawn.size(); i++) {
			Square squares = getSquare(moverPawn.get(i).x, moverPawn.get(i).y);
			Square squareE = getSquare(square.getPosition().x-1, square.getPosition().y+1);
			Square squareD = getSquare(square.getPosition().x-1, square.getPosition().y-1);
			Square squareF = getSquare(square.getPosition().x-1, square.getPosition().y);
			if((square.getPosition().y != 7 && squares == squareD && squareD.havePiece()) || (square.getPosition().y != 0 && squares == squareE && squareE.havePiece())) {
				if(squares.getPiece().getImagem().contains("Brown")) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareD && squares != squareE) {
				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
			}
		}
	}
	private void showPossibleMovesRook(Square square) {
		Rook rook = (Rook) square.getPiece();
		ArrayList<Point> moverRook = rook.getMover(square.getPosition().x, square.getPosition().y);
		int i = 0;
		Square squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		}
		squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		}	
		squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		}
		squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverRook.get(i).x, moverRook.get(i).y);
		}
				
	}
	private void showPossibleMovesBishop(Square square) {
		Bishop bishop = (Bishop) square.getPiece();
		ArrayList<Point> moverBishop = bishop.getMover(square.getPosition().x, square.getPosition().y);
		int i = 0;
		Square squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);		
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		}
		squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		}
		squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {	
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		}
		squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {	
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverBishop.get(i).x, moverBishop.get(i).y);
		}
	}
	private void showPossibleMovesKnight(Square square) {
		Knight knight = (Knight) square.getPiece();
		ArrayList<Point> moverKnight = knight.getMover(square.getPosition().x, square.getPosition().y);
		for(int i = 0; i < moverKnight.size(); i++) {
			Square squares = getSquare(moverKnight.get(i).x, moverKnight.get(i).y);
			if(square.getPiece().getImagem().contains("Yellow")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getImagem().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}		
			}
		}
	}
	private void showPossibleMovesQueen(Square square) {
		Queen queen = (Queen) square.getPiece();
		ArrayList<Point> moverQueen = queen.getMover(square.getPosition().x, square.getPosition().y);
		int i = 0;
		Square squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		
			if(squares.havePiece()) {;
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {;
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {	
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {	
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);	
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {	
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);		
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {
			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}
		squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		

			if(squares.havePiece())
			{
				if(square.getPiece().getImagem().contains("Yellow")) {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagem().contains("Brown")) {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				squares.setColor(this.colorMover);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(moverQueen.get(i).x, moverQueen.get(i).y);
		}	
	}
	private void showPossibleMovesKing(Square square) {
		King king = (King) square.getPiece();
		ArrayList<Point> moverKing = king.getMover(square.getPosition().x, square.getPosition().y);
		for(int i = 0; i < moverKing.size(); i++) {
			Square squares = getSquare(moverKing.get(i).x, moverKing.get(i).y);
			if(square.getPiece().getImagem().contains("Yellow")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagem().contains("Brown")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getImagem().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMover);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagem().contains("Yellow")) {
						squares.setColor(this.colorMover);
						possibleSquares.add(squares);
					}
				}		
			}
		}
	}			
	
	private void showPossibleMoves(Square square) {
		Piece piece = square.getPiece();
		if(piece.getTipo().equalsIgnoreCase("Pawn")) {
			if(piece.getImagem().contains("Yellow"))
				showPossibleMovesPawnY(square);
			if(piece.getImagem().contains("Brown"))
				showPossibleMovesPawnB(square);
		}
		if(piece.getTipo().equalsIgnoreCase("Rook")) {
			showPossibleMovesRook(square);
		}
		if(piece.getTipo().equalsIgnoreCase("Bishop")) {
			showPossibleMovesBishop(square);
		}
		if(piece.getTipo().equalsIgnoreCase("Knight")) {
			showPossibleMovesKnight(square);
		}
		if(piece.getTipo().equalsIgnoreCase("Queen")) {
			showPossibleMovesQueen(square);
		}
		if(piece.getTipo().equalsIgnoreCase("King")) {
			showPossibleMovesKing(square);
		}
	}		
}
