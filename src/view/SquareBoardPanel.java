package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Square;
import model.Piece;
import model.Pawn;
import model.Bishop;
import model.Rook;
import model.Knight;
import model.Queen;
import model.King;
import control.SquareControl;


public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.DARK_GRAY;
		Color colorTwo = Color.LIGHT_GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorMover = new Color(0x66B0FF);

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorMover);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		String piecePath = "icon/Brown P_48x48.png";
		Piece umPawn = new Pawn(piecePath,"Pawn");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(umPawn);
		}
		piecePath = "icon/Brown R_48x48.png";
		Piece umRook = new Rook(piecePath,"Rook");
		this.squareControl.getSquare(0, 0).setPiece(umRook);
		this.squareControl.getSquare(0, 7).setPiece(umRook);

		piecePath = "icon/Brown N_48x48.png";
		Piece umKnight = new Knight(piecePath,"Knight");
		this.squareControl.getSquare(0, 1).setPiece(umKnight);
		this.squareControl.getSquare(0, 6).setPiece(umKnight);

		piecePath = "icon/Brown B_48x48.png";
		Piece umBishop = new Bishop(piecePath,"Bishop");
		this.squareControl.getSquare(0, 2).setPiece(umBishop);
		this.squareControl.getSquare(0, 5).setPiece(umBishop);

		piecePath = "icon/Brown Q_48x48.png";
		Piece umaQueen = new Queen(piecePath,"Queen");
		this.squareControl.getSquare(0, 4).setPiece(umaQueen);

		piecePath = "icon/Brown K_48x48.png";
		Piece umKing = new King(piecePath,"King");
		this.squareControl.getSquare(0, 3).setPiece(umKing);

		
		piecePath = "icon/Yellow P_48x48.png";
		Piece umPawnY = new Pawn(piecePath,"Pawn");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(umPawnY);
		}
		
		piecePath = "icon/Yellow R_48x48.png";
		Piece umRookY = new Rook(piecePath,"Rook");
		this.squareControl.getSquare(7, 0).setPiece(umRookY);
		this.squareControl.getSquare(7, 7).setPiece(umRookY);

		piecePath = "icon/Yellow N_48x48.png";
		Piece umKnightY = new Knight(piecePath,"Knight");
		this.squareControl.getSquare(7, 1).setPiece(umKnightY);
		this.squareControl.getSquare(7, 6).setPiece(umKnightY);

		piecePath = "icon/Yellow B_48x48.png";
		Piece umBishopY = new Bishop(piecePath,"Bishop");
		this.squareControl.getSquare(7, 2).setPiece(umBishopY);
		this.squareControl.getSquare(7, 5).setPiece(umBishopY);

		piecePath = "icon/Yellow Q_48x48.png";
		Piece umaQueenY = new Queen(piecePath,"Queen");
		this.squareControl.getSquare(7, 4).setPiece(umaQueenY);

		piecePath = "icon/Yellow K_48x48.png";
		Piece umKingY = new King(piecePath,"King");
		this.squareControl.getSquare(7, 3).setPiece(umKingY);
	}
}
